<!DOCTYPE html>
<html lang="{{ $code }}">
    <head>
        <title>JSB</title>
        <meta charset="UTF-8" />
        @foreach($meta_tags as $key=>$value)
            <meta name="description" lang="{{ $key }}" content="{{ $value }}"/>
        @endforeach
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/zirafa/bootstrap-grid-only/94433673/css/grid12.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-HSQ3FGRTNE"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-HSQ3FGRTNE');
        </script>
    </head>

    <body class="antialiased">
        <div class="header">
            <div class="inner">
                <div class="container">
                    <div class="burger">
                        <div class="inner">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                    <div class="space_between"></div>
                    <div class="navbar">
                        <a href="{{ url('/lang').'/'.$code }}">{{ $menu[count($menu)-1]->value }}</a> 
                        @foreach($menu as $val)
                            @if($loop->index  == 0)
                                <a href="{{ url('/about').'/'.$code }}">{{ $val->value }}</a>
                            @endif
                            @if($loop->index == 1)
                                <a href="{{ url('/policy').'/'.$code }}">{{ $val->value }}</a> 
                            @endif
                            @if($loop->index == 2)

                                <a href="{{ url('/users').'/'.$code }}">{{ $val->value }}</a> 
                            @endif
                            @if($loop->index == 3)
                                <a href="{{ url('/rules').'/'.$code }}">{{ $val->value }}</a> 
                            @endif
                        @endforeach
                    </div>
                    <div class="spinner">
                        <div class="arrow">
                            <div class="header">
                                <img src="{{asset('storage/img/arrow.png')}}" alt="">
                                <span class="span currentLang">{{ $currentLang }}</span>
                            </div>
                            <div class="spinner_content">
                                <input type="hidden" name="lang" id="lang"/>
                                @yield('spinner')
                                
                                <input type="hidden" id="id">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="appear">
                <div class="navbar">
                        <a href="{{ url('/lang').'/'.$code }}">{{ $menu[count($menu)-1]->value }}</a> 
                        @foreach($menu as $val)
                            @if($loop->index  == 0)
                                <a href="{{ url('/about').'/'.$code }}">{{ $val->value }}</a>
                            @endif
                            @if($loop->index == 1)
                                <a href="{{ url('/policy').'/'.$code }}">{{ $val->value }}</a> 
                            @endif
                            @if($loop->index == 2)

                                <a href="{{ url('/users').'/'.$code }}">{{ $val->value }}</a> 
                            @endif
                            @if($loop->index == 3)
                                <a href="{{ url('/rules').'/'.$code }}">{{ $val->value }}</a> 
                            @endif
                        @endforeach
                </div>
            </div>
            @yield('content')
            
        </div>
    </body>
    <script type="text/javascript">
        
        let opened = false;
        let arrow_content = document.getElementsByClassName('arrow');
        let arrow = document.querySelector(".arrow img")
        let span = document.getElementsByClassName('span');
        let content = document.getElementsByClassName('spinner_content')
        let hiddenCode = document.getElementsByClassName('code')
        arrow_content[0].onclick = () =>{
            if(!opened) func('block','180')
            else func('none','0')
            opened = opened === false?true:false;
        }
        let func = (block,deg)=>{
            content[0].style.display = `${block}`
            arrow.style.transform = `rotate(${deg}deg)`
            // span[0].style.display = `${block === 'block'?'none':'inline-block'}`
        }
    </script>
    <script type="text/javascript">
        let isOpened = 0
        let spinner = document.getElementsByClassName('lang');
        let lang = document.getElementsByClassName('currentLang')[0];
        for(let x = 0;x < spinner.length;x++){
            if(lang.textContent.trim() === spinner[x].textContent.trim())
                spinner[x].classList.add('active')
        }
        function myFunction(x) {
            if (x.matches) { 
                $('.burger').addClass('flex-class')
                $('.container .navbar').removeClass('flex-class')
                $('.space_between').addClass('flex-class')
            } else {
                $('.burger').removeClass('flex-class')
                $('.container .navbar').addClass('flex-class')
                $('.appear').removeClass('flex-class')
                $('.space_between').removeClass('flex-class')
            }
        }

        var x = window.matchMedia("(max-width: 500px)")
        myFunction(x)
        x.addListener(myFunction) 
        $('.burger').on('click',()=>{
            if(isOpened === 1)
                $('.appear').removeClass('flex-class')
            else
                $('.appear').addClass('flex-class')
            isOpened = isOpened === 1? 0:1
        })
    </script>
</html>
