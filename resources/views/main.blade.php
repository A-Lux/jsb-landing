@extends('welcome')
@section('spinner')
    @foreach($langs as $lang)
        <a href="{{ url('/lang').'/'.$lang->code }}"><p class="lang">{{$lang->title}}</p></a>
        <input type='hidden' value='{{ $lang->code }}' class='code'/>
    @endforeach
@endsection
@section('content')
<style type="text/css">
    .navbar
    a:nth-child(1){
        background-color: white;
        color: #f73f3f
    }
</style>
<div class="container content" style="background-image: url({{asset('storage/img/background.png')}})">
    <div class="row">
        <div class="col-md-7 col-lg-7 content_inner">
            <div class="img">
                <img src="{{asset('storage/img/деньги.png')}}" alt="">
            </div>
            <div class="text">
                <p>
                    {{ $all_content["headerContent"] }}
                </p>
            </div>
            <div class="img">
                <img src="{{asset('storage/img/logo.png')}}" alt="">
                <p>
                    {{ $all_content["footerContent"] }}
                </p>
            </div>
            <div class="buttons ">
                <div>
                    <a href="{{ $all_content["play_market"] }}" target="_blank">
                        <img src="{{asset('storage/img/google.png')}}" alt="">
                    </a>
                </div>
                <div>
                    <a href="{{ $all_content["app_store"] }}" target="_blank">
                        <img src="{{asset('storage/img/iphone.png')}}" alt="">
                    </a>
                </div> 
            </div>
        </div>
    </div>
</div>
@endsection('content')
