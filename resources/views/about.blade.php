@extends('welcome')
<style>
    .clearfix{
        /* display: block !important */
    }
    .header 
    .content{
        background-size: 22% !important;
    }
    
    ul li{
        margin: 10px 0;
    }
    .contact-section .info-column .info-list li strong{
    color:#373a5b;
    font-size:24px;
    font-weight:600;
    display:block;
    margin-bottom:5px;
}

.contact-section .info-column .info-list li .icon{
    position:absolute;
    content:'';
    left:0px;
    top:7px;
    font-size:36px;
    line-height:1em;

    background: linear-gradient(to right, #2bb852 0%, #6ede4c 98%, #6ede4c 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
}
.topapp-sec-title .title:before{
    display: none !important;
}

    .contact-section .info-column .info-list li:nth-child(2) .icon{
        background: linear-gradient(to right, #9828d5 0%, #de3fdb 98%, #de3fdb 100%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
    }

    .contact-section .info-column .info-list li:nth-child(3) .icon{
        background: linear-gradient(to right, #0084fd 0%, #69d6ff 98%, #69d6ff 100%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
    }

    .contact-section .info-column .info-list li a{
        position:relative;
        color:#373a5b;
        font-size:16px;
    }
    .content{
        padding: 5% 0;
    }
    .topapp-sec-title h2{
        font-size: 18px !important;
    }
    .navbar
    a:nth-child(2){
        background-color: white;
        color: #f73f3f
    }
    .topapp-sec-title .title{
        margin-left: 0 !important;
    }
    .info-list{
        margin:  0 !important;
        padding: 5px !important
    }
</style>
@section('spinner')
    @foreach($langs as $lang)
        <a href="{{ url('/about').'/'.$lang->code }}"><p class="lang">{{$lang->title}}</p></a>
        <input type='hidden' value='{{ $lang->code }}' class='code'/>
    @endforeach
@endsection
@section('content')
<div class="container content" style="background-image: url({{asset('storage/img/background.png')}})">
    <div class="row">
        <div class="col-md-8 col-lg-8 content_inner">
            <div class="form-column ">
                <div class="inner-column">
                    <div class="topapp-sec-title style-three">
                        <div class="title">{{ $all_content["contactContent"] }}</div>
                        <h2>{{ $all_content["headerContent"] }}</h2>
                    </div>
                    <div class="default-form">   
                    </div>
                    
                </div>
            </div>
            <div class="row clearfix">
                <div class="lg-col-12">
                    <div class="inner-column">
                        <div class="patern-layer-one" style="background-image: url(http://www.iprize.io/wp-content/themes/appilo/img/topapp/background/pattern-12.png)"></div>
                        <div class="patern-layer-two" style="background-image: url(http://www.iprize.io/wp-content/themes/appilo/img/topapp/background/pattern-13.png)"></div>
                        <div class="patern-layer-three paroller" data-paroller-factor="-0.10" data-paroller-factor-lg="0.08" data-paroller-type="foreground" data-paroller-direction="horizontal" style="background-image: url(&quot;http://www.iprize.io/wp-content/themes/appilo/img/topapp/background/pattern-14.png&quot;); transform: translateX(-111px); transition: transform 0s linear 0s; will-change: transform;"></div>
                        <ul class="info-list">
                            <li><span class="icon flaticon-address"></span> <strong>{{ $all_content["officeContent"] }}:</strong>
                                <!-- 'email','phones','addressContent' -->
                                {{ $all_content["addressContent"] }}
                            </li>
                            <li><span class="icon flaticon-phone"></span><strong>{{ $all_content["phoneContent"]  }}:</strong>
                                {{ $all_content["phones"] }}
                            </li>
                            <li><span class="icon flaticon-invite"></span> <strong>{{ $all_content["mailContent"]  }}:</strong>
                                {{ $all_content["email"] }}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')