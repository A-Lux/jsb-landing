@extends('welcome')
@section('spinner')
    @foreach($langs as $lang)
        <a href="{{ url('/policy').'/'.$lang->code }}"><p class="lang">{{$lang->title}}</p></a>
        <input type='hidden' value='{{ $lang->code }}' class='code'/>
    @endforeach
@endsection
@section('content')
<style>
    .text p{
        font-size: 14px !important;
        text-align: justify;
    }
    .navbar
    a:nth-child(3){
        background-color: white;
        color: #f73f3f
    }
</style>
<div class="container content">
<!-- style="background-image: url({{asset('storage/img/background.png')}})" -->
    <div class="row">
        <div class="col-md-12 col-lg-12 content_inner">
            <div class="text">
                <input type="hidden" value="{{ $policyContent['content'] }}" name="" class="hiddenVal">
                <p class="p_text">
                    
                </p>
            </div>
        </div>
    </div>
</div>
<script>
        $(function(){
            let item = document.getElementsByClassName('p_text')[0];
            let text = document.getElementsByClassName('hiddenVal')[0].value;
            let ar = text.split("\n");
            let newText = ""
            for(let x = 0;x < ar.length;x++){
                newText += ar[x] + "<br/>"
            }
            $('.p_text').html(newText)
        })

    </script>
@endsection('content')