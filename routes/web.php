<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/','/lang/en');

Route::get('/lang/{lang}', 'Controller@index');
Route::get('/about/{lang}', 'Controller@about');
Route::get('/rules/{lang}', 'Controller@rules');
Route::get('/users/{lang}', 'Controller@users');
Route::get('/policy/{lang}', 'Controller@policy');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
