<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use TCG\Voyager\Traits\Translatable;

class ContestPage extends Model
{
    use HasFactory,Translatable;
    protected $translatable = ['content'];

}
