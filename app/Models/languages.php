<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class languages extends Model
{
    use HasFactory;

    public function select_language(){
        return $this->hasOne('App\Models\select_language');
    }
    
}
