<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class MainPage extends Model
{
    use HasFactory,Translatable;
    protected $translatable = ['headerContent', 'footerContent', 'app_store', 'play_market'];

}
