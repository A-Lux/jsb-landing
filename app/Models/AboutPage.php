<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class AboutPage extends Model
{
    use HasFactory,Translatable;
    protected $translatable = ['contactContent', 'headerContent','mailContent','phoneContent','officeContent','email','phones','addressContent'];

}
    