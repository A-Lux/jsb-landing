<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class MainController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function index(Request $request)
    {
        return $this->show($request, 1);
    }
}
