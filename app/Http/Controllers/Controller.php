<?php

namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Models\MenuItem;
use App\Models\languages;
use App\Models\MainPage;
use App\Models\PrivacyPage;
use App\Models\TermsPage;
use App\Models\ContestPage;
use App\Models\AboutPage;
use App\Models\select_language;
use TCG\Voyager\Facades\Voyager;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    private $ar; 
    private $selected_langs;
    private $langsId;
    private $currentLang;
    private $content;
    private $translatedCode;

    function __construct(){
        $this->ar = array();
        $this->langsId = array();
        $this->selected_langs = DB::table('languages');
        $this->content = array();
        $this->translatedCode = array();
    }

    function findCurrent($lang){
        foreach($this->selected_langs as $val){
            if($val->code === $lang){
                return $val->title;
            }
        }
    }

    function index(Request $request,$lang){
        $menus = MenuItem::withTranslation($lang)->get();
        $this->content = MainPage::withTranslation($lang)->get()->first();
        if($lang === 'en'){
            foreach($menus as $item)
                if($item->menu_id == 2) {
                    $item["value"] = $item->title;
                    array_push($this->ar,$item);
                }
        }
        $item = MainPage::withTranslation($lang)->get()->first()["translations"];
        foreach($item as $val)  $this->content[$val->column_name] = $val->value;
        
        foreach($menus as $item)
            if($item->menu_id == 2) {
                foreach($item->translations as $val)    
                    array_push($this->ar,$val);
            }
        
        foreach(select_language::all() as $val){
            array_push($this->langsId,$val["lang_id"]);
        }
        $this->selected_langs = $this->selected_langs
                                    ->whereIn('id',$this->langsId)
                                    ->get();

        $langs = languages::all();
        foreach($langs as $val){
            $in = MainPage::withTranslation($val->code)->get()->first()["translations"];
            if(count($in) == 0) {
                $in = MainPage::withTranslation($lang)->get();
                $this->translatedCode[$val->code] = $in[0]->headerContent;
                continue;
            }
            $this->translatedCode[$val->code] = $in[2]->value;
        }
        
        
        $this->currentLang = $this->findCurrent($lang);
        return view('main',[
            'langs'=>$this->selected_langs,
            'currentLang'=>$this->currentLang,
            'code'=>$lang,
            'all_content' => $this->content,
            'menu'=>$this->ar,
            'meta_tags'=>$this->translatedCode]);
    }
    
    function about($lang){
        $menus = MenuItem::withTranslation($lang)->get();
        $this->content = AboutPage::withTranslation($lang)->get()->first();
        if($lang == 'en'){
            foreach($menus as $item)
                if($item->menu_id == 2) {
                    $item["value"] = $item->title;
                    array_push($this->ar,$item);
                }
        }
        $item = AboutPage::withTranslation($lang)->get()->first()["translations"];
        foreach($item as $val)  $this->content[$val->column_name] = $val->value;
        foreach($menus as $value)
            if($value->menu_id == 2) 
                foreach($value->translations as $val)    
                    array_push($this->ar,$val);

        foreach(select_language::all() as $val) array_push($this->langsId,$val["lang_id"]);
        
        $this->selected_langs = $this->selected_langs
                                    ->whereIn('id',$this->langsId)
                                    ->get();

        $langs = languages::all();
        foreach($langs as $val){
            $in = MainPage::withTranslation($val->code)->get()->first()["translations"];
            if(count($in) == 0) {
                $in = MainPage::withTranslation($lang)->get();
                $this->translatedCode[$val->code] = $in[0]->headerContent;
                continue;
            }
            $this->translatedCode[$val->code] = $in[2]->value;
        }

        foreach($this->selected_langs as $val){
            if($val->code === $lang){
                $this->currentLang = $val->title;
                break;
            }
        }
        return view('about',[
            'langs'=>$this->selected_langs,
            'currentLang'=>$this->currentLang,
            'code'=>$lang,
            'all_content'=>$this->content,
            'menu'=>$this->ar,
            'meta_tags'=>$this->translatedCode]);
    }
    function policy($lang){
        $menus = MenuItem::withTranslation($lang)->get();
        $this->content = PrivacyPage::withTranslation($lang)->get()->first();
        if($lang == 'en'){
            foreach($menus as $item)
                if($item->menu_id == 2) {
                    $item["value"] = $item->title;
                    array_push($this->ar,$item);
                }
        }
        $item = PrivacyPage::withTranslation($lang)->get()->first()["translations"];
        foreach($item as $val)  $this->content[$val->column_name] = $val->value;
        
        foreach($menus as $value)
            if($value->menu_id == 2) 
                foreach($value->translations as $val)    
                    array_push($this->ar,$val);
        
        foreach(select_language::all() as $val){
            array_push($this->langsId,$val["lang_id"]);
        }
        $this->selected_langs = $this->selected_langs
                                    ->whereIn('id',$this->langsId)
                                    ->get();

        $langs = languages::all();
        foreach($langs as $val){
            $in = MainPage::withTranslation($val->code)->get()->first()["translations"];
            if(count($in) == 0) {
                $in = MainPage::withTranslation($lang)->get();
                $this->translatedCode[$val->code] = $in[0]->headerContent;
                continue;
            }
            $this->translatedCode[$val->code] = $in[2]->value;
        }

        $this->currentLang = $this->findCurrent($lang);
        return view('policy',[
            'langs'=>$this->selected_langs,
            'currentLang'=>$this->currentLang,
            'code'=>$lang,
            'policyContent' => $this->content,
            'menu'=>$this->ar,
            'meta_tags'=>$this->translatedCode]);
    }
    function rules($lang){
        $menus = MenuItem::withTranslation($lang)->get();
        $this->content = ContestPage::withTranslation($lang)->get()->first();
        if($lang == 'en'){
            foreach($menus as $item)
                if($item->menu_id == 2) {
                    $item["value"] = $item->title;
                    array_push($this->ar,$item);
                }
        }

        $item = ContestPage::withTranslation($lang)->get()->first()["translations"];
        foreach($item as $val)  $this->content[$val->column_name] = $val->value;

        foreach($menus as $item)
            if($item->menu_id == 2) 
                foreach($item->translations as $val)    
                    array_push($this->ar,$val);
        
        foreach(select_language::all() as $val){
            array_push($this->langsId,$val["lang_id"]);
        }
        $this->selected_langs = $this->selected_langs
                                    ->whereIn('id',$this->langsId)
                                    ->get();

        $langs = languages::all();
        foreach($langs as $val){
            $in = MainPage::withTranslation($val->code)->get()->first()["translations"];
            if(count($in) == 0) {
                $in = MainPage::withTranslation($lang)->get();
                $this->translatedCode[$val->code] = $in[0]->headerContent;
                continue;
            }
            $this->translatedCode[$val->code] = $in[2]->value;
        }

        $this->currentLang = $this->findCurrent($lang);
        
        return view('rules',[
            'langs'=>$this->selected_langs,
            'currentLang'=>$this->currentLang,
            'code'=>$lang,
            'rulesContent' => $this->content,
            'menu'=>$this->ar,
            'meta_tags'=>$this->translatedCode]);
    }
    function users($lang){
        $menus = MenuItem::withTranslation($lang)->get();
        $this->content = TermsPage::withTranslation($lang)->get()->first();
        if($lang == 'en'){
            foreach($menus as $item)
                if($item->menu_id == 2) {
                    $item["value"] = $item->title;
                    array_push($this->ar,$item);
                }
        }

        $item = TermsPage::withTranslation($lang)->get()->first()["translations"];
        foreach($item as $val)  $this->content[$val->column_name] = $val->value;
        
        $menus = MenuItem::withTranslation($lang)->get();
        foreach($menus as $item)
            if($item->menu_id == 2) 
                foreach($item->translations as $val)    
                    array_push($this->ar,$val);
        
        foreach(select_language::all() as $val){
            array_push($this->langsId,$val["lang_id"]);
        }
        $this->selected_langs = $this->selected_langs
                                    ->whereIn('id',$this->langsId)
                                    ->get();
        $langs = languages::all();
        foreach($langs as $val){
            $in = MainPage::withTranslation($val->code)->get()->first()["translations"];
            if(count($in) == 0) {
                $in = MainPage::withTranslation($lang)->get();
                $this->translatedCode[$val->code] = $in[0]->headerContent;
                continue;
            }
            $this->translatedCode[$val->code] = $in[2]->value;
        }
        $this->currentLang = $this->findCurrent($lang);
        return view('users',[
            'langs'=>$this->selected_langs,
            'currentLang'=>$this->currentLang,
            'code'=>$lang,
            'usersContent' => $this->content,
            'menu'=>$this->ar,
            'meta_tags'=>$this->translatedCode]);
    }
}
